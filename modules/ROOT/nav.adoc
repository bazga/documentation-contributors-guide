//* xref:organizational/index.adoc[How we work]
* How we work
** xref:organizational/charter.adoc[The Docs Charter]
** xref:organizational/meetings.adoc[Docs Meetings]
** xref:organizational/workflow.adoc[Docs Workflow organization]

* xref:contributing-docs/index.adoc[Contribute to improve and expand Docs articles]

** Contributing to existing documentation 
// xref:contributing-docs/contrib-existing-documentation.adoc[Contributing to existing documentation]
** xref:contributing-docs/contrib-new-documentation.adoc[Create a new documentation module]
** xref:contributing-docs/contrib-quickdocs.adoc[Contributing to Quick Docs]
** xref:contributing-docs/contrib-release-notes.adoc[Contributing to Release Notes]

** Contribution Tools
*** xref:contributing-docs/tools-gitlab-howto.adoc[HowTo for casual contributions (for GitLab-based pages)]
*** xref:contributing-docs/tools-web-ide-ui.adoc[How to profoundly use GitLab UI for document maintenance]
//*** xref:contributing-docs/contributions-quick-repo.adoc[How to contribute to Quick Docs repo]
*** xref:contributing-docs/tools-file-edit-pagure.adoc[How to edit files on the Pagure Web UI]
*** xref:contributing-docs/tools-local-authoring-env.adoc[How to create and use a local Fedora authoring environment]
*** xref:contributing-docs/tools-localenv-preview.adoc[How to run a local preview]
*** xref:contributing-docs/tools-vale-linter.adoc[How to check documentation style with Vale]  

** xref:contributing-docs/style-guide.adoc[The docs style guide]
** xref:contributing-docs/asciidoc-intro.adoc[AsciiDoc for Fedora]
*** xref:contributing-docs/asciidoc-markup.adoc[Markup]
*** xref:contributing-docs/asciidoc-reusable-attributes.adoc[Reusable attributes]
// ** xref:contributing-docs/translations.adoc[Working with Translations]
** Working with Translations

//* Contribute to keeping Docs up and running
* xref:contributing-infra/index.adoc[Contribute to keeping docs up and running]
** xref:contributing-infra/design-ux-contribution.adoc[Design the user interface for Docs]

//* xref:archive/index.adoc[What we have achieved so far]
//* What we have achieved so far
